#!/usr/bin/env python3

import requests
import json
import argparse


def parse_args():
    parser = argparse.ArgumentParser(description="Query virustotal.com hashes")

    parser.add_argument("key", help='api key for virustotal.com')
    parser.add_argument("filehash", help='filehash you want to look for')
    args = parser.parse_args()
    return(args)


def main(api_key, filehash):
    ''' Queries virustotal.com api with the key and hash provided.
        Returns only the results in which the hash was detected. '''
    data = {'apikey': api_key, 'resource': filehash}
    url = 'https://www.virustotal.com/vtapi/v2/file/report'
    r = requests.get(url=url, params=data)
    response = json.loads(r.text)
    scans = response['scans']
    output = {}  # this will store the results
    for scan, results in scans.items():
        if results['detected']:
            output[scan] = results
    json_output = json.dumps(output)
    return(json_output)


if __name__ == '__main__':
    args = parse_args()
    print(main(args.key, args.filehash))
